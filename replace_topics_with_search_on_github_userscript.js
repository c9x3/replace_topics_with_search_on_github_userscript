// ==UserScript==
// @name                [C9x3] Replace Topics with Search on Github UserScript
// @description         Tries to replace Topics with Search on GitHub.
//                      You CAN disable parts of this UserScript by marking the parts you want disabled as a comment!
// @match               *://github.com/*
// @version             0.7
// @run-at              document-start
// @grant               none
// ProjectHomepage      https://c9x3.neocities.org/
// @downloadURL  		https://gitlab.com/c9x3/replace_topics_with_search_on_github_userscript/-/raw/main/replace_topics_with_search_on_github_userscript.js
// @updateURL    		https://gitlab.com/c9x3/replace_topics_with_search_on_github_userscript/-/raw/main/replace_topics_with_search_on_github_userscript.js
// ==/UserScript==

//

// Make a log in the console when the script starts. 

console.log('Replace topics with search on Github UserScript has started!')

//

// Don’t run in frames.

if (window.top !== window.self)	  

return;

var currentURL = location.href;

//

// Try to replace, "/-/tree/main" with, " ."

if (currentURL.match("/topics/")) {
  
	location.href = location.href.replace("/topics/", "/search?q=");
  
};

//

